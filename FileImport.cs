﻿using System;
using System.Data;
using System.IO;
using System.Reflection;

namespace DataCaptureApp
{

    public static class FileImport
    {
        public static DataTable Import()
        {
            DataTable dataTable = new DataTable();
            int counter = 0;

            try
            {
                dataTable.Columns.Add("InvoiceNumber", typeof(string));
                dataTable.Columns.Add("InvoiceDate", typeof(DateTime));
                dataTable.Columns.Add("Address", typeof(string));
                dataTable.Columns.Add("InvoiceTotal", typeof(float));
                dataTable.Columns.Add("Description", typeof(string));
                dataTable.Columns.Add("Quantity", typeof(float));
                dataTable.Columns.Add("UnitSellingPriceExVAT", typeof(float));

                //string filePath = @"C:\Users\LENOVO-THINKPAD\Source\Repos\DataCaptureApp\bin\data.csv";

                //string folder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                //string filePath = Path.Combine(folder, "data.csv"); //'C:\Users\LENOVO-THINKPAD\Source\Repos\Console-Apps\DataCaptureApp\bin\Debug\DataCaptureApp\bin\data.csv'.'

                string filePath = "..\\netcoreapp3.1\\data.csv";

                using (StreamReader strRead = new StreamReader(filePath))
                {
                    while (strRead.Peek() != -1)
                    {
                        string s = strRead.ReadLine();
                        string[] data = s.Split(",");

                        if (counter > 0)
                            dataTable.Rows.Add(data[0], DateTime.ParseExact(data[1].Trim(), @"dd/MM/yyyy hh:mm",
    System.Globalization.CultureInfo.InvariantCulture), (string.IsNullOrEmpty(data[2]).Equals(false) ? data[2] : ""), Convert.ToDouble(data[3]), data[4], Convert.ToDouble(data[5]), Convert.ToDouble(data[6]));
                        counter++;
                    }
                }
            }
            catch (Exception ex)
            {

                UIDisplay.LogExceptions(ex.Message + " Error occurrence: Import Method");
            }
            return dataTable;
        }

    }
}
