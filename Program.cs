﻿namespace DataCaptureApp
{
    using System;
    using System.Data;
    using System.Diagnostics;
    using System.IO;

    internal class Program
    {
        internal static void Main(string[] args)
        {
            DataTable dataTable = new DataTable();
            string space = string.Empty;

            char response = UIDisplay.Home();

            if (response.Equals('Y') || response.Equals('y'))
            {
                // File Import
                dataTable = FileImport.Import();

                // Saving Data From the DataTable to the Database
                string message = HelperManager.SaveInvoice(dataTable);

                if (string.IsNullOrEmpty(message).Equals(false))
                {

                    // If data already exist prompt user to view the data
                    if (message.Contains("Data Already Exist"))
                    {
                        char readDataResponse = UIDisplay.PromptReadData();
                        if (readDataResponse.Equals('Y') || readDataResponse.Equals('y'))
                        {
                            UIDisplay.InvoiceQuantityHeader();
                            HelperManager.InvoiceQuantity();
                            HelperManager.BalanceTotals();
                        }
                        else
                        {
                            Console.WriteLine(Environment.NewLine);
                            UIDisplay.MessageBox("Thank you for using our system. Good day.");
                        }                       
                    }
                    else
                    {
                        UIDisplay.LogExceptions(message);
                    }
                }
                else
                {
                    UIDisplay.InvoiceQuantityHeader();
                    HelperManager.InvoiceQuantity();

                    // Displaying Process Result
                    UIDisplay.FileImportResult("Result: Invoice File Successfully Save...!");

                    HelperManager.BalanceTotals();
                }
            }
            else
            {
                // Displaying Process Cancellation
                UIDisplay.FileImportResult("Import process canceled...");
            }

            Console.ReadKey();
        }
    }
}
