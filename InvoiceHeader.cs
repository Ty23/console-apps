﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace DataCaptureApp
{
    public partial class InvoiceHeader
    {
        [Key]
        public int InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Address { get; set; }
        public float InvoiceTotal { get; set; }
    }
}
