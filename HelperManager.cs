﻿namespace DataCaptureApp
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    public static class HelperManager
    {
        private static readonly EnitiesContext dbContext = new EnitiesContext();
        public static void CheckDatabaseConnection()
        {
            if (!dbContext.Database.CanConnect())
            {
                var space = string.Empty;
                UIDisplay.LogExceptions(Environment.NewLine + space.PadLeft(40) + "Database Connection Does Not Exist.\nPlease either run EF migration or run SQL database script found in the project's database folder to create the database and re-run the application." + "\n");
            }          
        }
        public static List<InvoiceDto> ReadInvoiceData()
        {
            CheckDatabaseConnection();
            try
            {
                var lstInvoiceDto = dbContext.InvoiceHeaders.Join(dbContext.InvoiceLines,
                h => h.InvoiceNumber,
                l => l.InvoiceNumber,
                (h, l) => new InvoiceDto
                {
                    InvoiceId = h.InvoiceId,
                    InvoiceDate = h.InvoiceDate,
                    InvoiceNumber = h.InvoiceNumber,
                    Address = h.Address,
                    InvoiceTotal = Math.Round((Decimal)h.InvoiceTotal, 2),
                    Description = l.Description,
                    Quantity = (int)l.Quantity,
                    UnitSellingPriceExVAT = Math.Round((Decimal)l.UnitSellingPriceExVAT, 2)
                }).OrderBy(c => c.InvoiceNumber).ToList();

                return lstInvoiceDto;
            }
            catch (Exception ex)
            {
                UIDisplay.LogExceptions(ex.Message + " Error occurrence: ReadInvoiceData Method");
                return null;
            }      
        }

        public static string SaveInvoice(DataTable datatable)
        {
            List<InvoiceHeader> lstInvoiceHeader = new List<InvoiceHeader>();
            List<InvoiceLines> lstInvoiceLines = new List<InvoiceLines>();

            DateTime date = DateTime.Now;
            string msg = string.Empty;

            CheckDatabaseConnection();
            try
            {
                datatable.AcceptChanges();

                lstInvoiceHeader = (from row in datatable.AsEnumerable().AsQueryable()
                                    select new InvoiceHeader
                                    {
                                        InvoiceNumber = row.Field<string>("InvoiceNumber"),
                                        InvoiceDate = row.Field<DateTime>("InvoiceDate"),
                                        Address = row.Field<string>("Address"),
                                        InvoiceTotal = (float)row.Field<float>("InvoiceTotal"),
                                    }).ToList();

                foreach (var invoiceHeader in lstInvoiceHeader)
                {
                    if (dbContext.InvoiceHeaders.Any(x => x.InvoiceNumber == invoiceHeader.InvoiceNumber))
                    {
                        msg = "Data Already Exist";
                    }
                    else
                    {
                        dbContext.InvoiceHeaders.Add(invoiceHeader);
                    }
                }
                //dbContext.InvoiceHeaders.AddRange(lstInvoiceHeader);
                try
                {
                    lstInvoiceLines = (from row in datatable.AsEnumerable().AsQueryable()
                                       select new InvoiceLines
                                       {
                                           InvoiceNumber = row.Field<string>("InvoiceNumber"),
                                           Description = row.Field<string>("Description"),
                                           Quantity = (float)row.Field<float>("Quantity"),
                                           UnitSellingPriceExVAT = (float)row.Field<float>("UnitSellingPriceExVAT"),
                                       }).ToList();

                    foreach (var invoiceLine in lstInvoiceLines)
                    {
                        if (dbContext.InvoiceLines.Any(x => x.InvoiceNumber == invoiceLine.InvoiceNumber))
                        {
                            msg = "Data Already Exist";
                        }
                        else
                        {
                            dbContext.InvoiceLines.Add(invoiceLine);
                        }
                    }
                    //dbContext.InvoiceLines.AddRange(lstInvoiceLines);
                }
                catch (Exception ex)
                {
                    msg = ex.Message + " Error occurrence: SaveInvoiceLines";
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message + " Error occurrence: SaveInvoiceHeader Method";
            }
            finally
            {
                if (string.IsNullOrEmpty(msg))
                {
                    dbContext.SaveChanges();
                }
            }
            return msg;
        }

        //Method to get Invoice No & Total Quantity to display
        public static void InvoiceQuantity()
        {
            CheckDatabaseConnection();

            var lstInvoiceDto = HelperManager.ReadInvoiceData();
            string strInvoiceNo = string.Empty;
            bool checkInvoice = false;
            try
            {
                //Console.WriteLine(Environment.NewLine);
                foreach (var invoice in lstInvoiceDto)
                {
                    checkInvoice = strInvoiceNo != invoice.InvoiceNumber ? true : false;
                    strInvoiceNo = invoice.InvoiceNumber;
                    if (checkInvoice)
                    {
                        UIDisplay.InvoiceQuantityTable(invoice.InvoiceNumber, dbContext.InvoiceLines.Where(x => x.InvoiceNumber == invoice.InvoiceNumber).Sum(x => x.Quantity));
                    }
                }
            }
            catch (Exception ex)
            {
                UIDisplay.LogExceptions(ex.Message + " Error occurrence: InvoiceQuantity Method");
            }        
        }

        public static void BalanceTotals()
        {
            CheckDatabaseConnection();

            DataTable dataTable = FileImport.Import();
            string balanceResult = "";

            try
            {
                //var fileTotal = dataTable.AsEnumerable().Sum(x => x.Field<float>("UnitSellingPriceExVAT"));
                //var  fileInTotal = dataTable.AsEnumerable().Sum(x => x.Field<float>("InvoiceTotal"));

                var lstFileTotals = dataTable.AsEnumerable().Select(x => new { Totals = (x.Field<float>("Quantity") * x.Field<float>("UnitSellingPriceExVAT")) }).Sum(x => x.Totals);

                float dbInTotal = dbContext.InvoiceHeaders.Sum(x => x.InvoiceTotal);

                if (lstFileTotals == dbInTotal)
                {
                    balanceResult = "File and Database Totals Setoff or balanced";
                }
                else
                {
                    balanceResult = "File and Database Totals failed balance .... !";
                }

                UIDisplay.BalanceReport(dbInTotal, lstFileTotals, balanceResult);
            }
            catch (Exception ex)
            {

                UIDisplay.LogExceptions(ex.Message + " Error occurrence: InvoiceQuantity Method");                
            }    
        }
    }
}
