﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCaptureApp
{
    public static class UIDisplay
    {
        public static char Home()
        {
            string space = string.Empty;

            Console.WriteLine(space.PadLeft(40) + ".....Welcome to csv Import System....." + Environment.NewLine);
            Console.WriteLine(space.PadLeft(25) + "Please Press Y for Yes to start the upload process and N for No to cancel?");
            char response = Convert.ToChar(Console.Read());

            return response;
        }

        public static void InvoiceQuantityHeader()
        {
            string space = string.Empty;
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine(space.PadLeft(40) + "Invoice Number" + space.PadRight(5) + "Total Quantity");
            Console.WriteLine(space.PadLeft(40) + "--------------" + space.PadLeft(5) + "--------------" + Environment.NewLine);
        }

        public static void InvoiceQuantityTable(string strInvoiceNo,float quantity)
        {
            string space = string.Empty;
            Console.WriteLine(space.PadLeft(45) + strInvoiceNo + space.PadLeft(16) + quantity);
        }

        public static void FileImportResult(string message)
        {
            string space = string.Empty;
            Console.WriteLine(Environment.NewLine + space.PadLeft(40) + message + Environment.NewLine);
        }

        public static void BalanceReport(float dbInTotal, float lstFileTotals, string balanceResult)
        {
            string space = string.Empty;
           
            Console.WriteLine(Environment.NewLine + space.PadLeft(40) + "Database Invoice Balance" + space.PadLeft(5) + ": R "  + dbInTotal);
            Console.WriteLine(space.PadLeft(40) + "CSV Invoice Balance" + space.PadLeft(10) + ": R "+ lstFileTotals + Environment.NewLine);

            Console.WriteLine(space.PadLeft(30) + "File & Database Balance Result   :" + balanceResult + Environment.NewLine);
        }

        public static void LogExceptions(string error)
        {
            string space = string.Empty;

            Console.WriteLine(space.PadLeft(40) + "Logged Error");
            Console.WriteLine(space.PadLeft(40) +  error);
        }
        public static char PromptReadData()
        {
            string space = string.Empty;
            Console.WriteLine(space.PadLeft(25) + "Data Already Exist. Do you want to read the data? Press Y for Yes and N for No");

            var response = Console.ReadKey().KeyChar;
            return response;
        }
        public static void MessageBox(string message)
        {
            string space = string.Empty;
            Console.WriteLine(space.PadLeft(40) + message + Environment.NewLine);
        }
    }
}
