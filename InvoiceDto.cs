﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCaptureApp
{
    public class InvoiceDto
    {
        public int InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Address { get; set; }
        public decimal InvoiceTotal { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal UnitSellingPriceExVAT { get; set; }

    }
}
