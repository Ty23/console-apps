﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DataCaptureApp
{
    public class InvoiceLines 
    {
        [Key]
        public int LineId { get; set; }
        public string InvoiceNumber { get; set; }
        public string Description { get; set; }
        public float Quantity { get; set; }
        public float UnitSellingPriceExVAT { get; set; }
    }
}
